import sqlite3
import os


def checkConditions(cursor):
    dbExist = os.path.isfile('schedule.db')
    cursor.execute('SELECT * FROM courses')
    courses = cursor.fetchall()
    return dbExist and len(courses) > 0


def deductStudents(cursor, course):
    num_of_seats = course[3]
    cursor.execute("SELECT * FROM students WHERE grade=?",(course[2],))
    student = cursor.fetchone()
    if num_of_seats >= student[1]:
        cursor.execute("UPDATE students SET count=0 WHERE grade=?",(student[0],))
        dbcon.commit()
    else:
        cursor.execute("UPDATE students SET count=count-? WHERE grade=?",(num_of_seats, student[0]))
        dbcon.commit()



def assignClassroom(cursor, classroom, iterationNum):
    cursor.execute("SELECT * FROM courses")
    courses = cursor.fetchall()
    for course in courses:
        if course[4] == classroom[0]:
            cursor.execute("UPDATE classrooms SET current_course_id=?, current_course_time_left=? WHERE id=?",(course[0],course[5],classroom[0]))
            dbcon.commit()

            deductStudents(cursor,course)
            print("({}) {}: {} is schedule to start".format(iterationNum, classroom[1], course[1]))
            break



def finishCourse(cursor, classroom, iterationNum):
    cursor.execute("SELECT * FROM courses WHERE id=?", (classroom[2],))
    course = cursor.fetchone()

    print("({}) {}:{} is done".format(iterationNum,classroom[1],course[1]))

    cursor.execute("DELETE FROM courses WHERE id=?", (classroom[2],)) # deleteing the course since its done
    cursor.execute("UPDATE classrooms SET current_course_id=0 WHERE id=" + str(classroom[0]))
    dbcon.commit()



def occupiedClassroom(cursor, classroom, iterationNum):
    cursor.execute("SELECT * FROM courses WHERE id=?", (classroom[2],))
    course = cursor.fetchone()

    print("({}) {}: occupied by {}".format(iterationNum,classroom[1],course[1]))



def print_tables(cursor):
    cursor.execute("SELECT * FROM courses")
    courses = cursor.fetchall()
    print("courses")
    for course in courses:
        print(course)

    cursor.execute("SELECT * FROM classrooms")
    classrooms = cursor.fetchall()
    print("classrooms")
    for classroom in classrooms:
        print (classroom)

    cursor.execute("SELECT * FROM students")
    students = cursor.fetchall()
    print("students")
    for student in students:
        print(student)








if os.path.isfile('schedule.db'):

    dbcon = sqlite3.connect('schedule.db')
    cursor = dbcon.cursor()
    iterationNum = 0

    cursor.execute('SELECT * FROM classrooms')
    classrooms = cursor.fetchall()

    while(checkConditions(cursor)):
        cursor.execute('SELECT * FROM classrooms')
        classrooms = cursor.fetchall()
        for classroom in classrooms:
            if classroom[3] != 0:
                cursor.execute("UPDATE classrooms SET current_course_time_left=current_course_time_left - 1 WHERE id=?",(classroom[0],))
                dbcon.commit()
            if classroom[3] <= 1:
                if classroom[2] != 0:
                    finishCourse(cursor, classroom, iterationNum)
                assignClassroom(cursor, classroom, iterationNum)

            else:
                occupiedClassroom(cursor,classroom, iterationNum)
        print_tables(cursor)
        iterationNum += 1

cursor.close()
dbcon.close()




