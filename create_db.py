import sqlite3
import os.path
import sys


def createTables(conn):
    conn.executescript("""
        CREATE TABLE students
        (
            grade TEXT PRIMARY KEY,
            count INTEGER NOT NULL
        );
    
        CREATE TABLE classrooms
        (
            id INTEGER NOT NULL,
            location TEXT NOT NULL,
            current_course_id INTEGER NOT NULL,
            current_course_time_left INTEGER NOT NULL
        );
    
    
        CREATE TABLE courses
        (
            id INTEGER PRIMARY KEY,
            course_name TEXT NOT NULL,
            student TEXT NOT NULL,
            number_of_students INTEGER NOT NULL,
            class_id INTEGER REFERENCES classrooms(id),
            course_length INTEGER NOT NULL
        );
    
    """)
    conn.commit()


def insertStudent(grade, count,conn):
    conn.execute("""
        INSERT INTO students 
        VALUES (?,?) """, (grade,count))

    conn.commit()



def insertClassroom(id, location,conn):
    conn.execute("""
        INSERT INTO classrooms
        VALUES (?,?,0,0)""",(id,location.replace("\n","")))

    conn.commit()


def insertCourse(id, course_name, student, number_of_students, class_id, course_length, conn):
    conn.execute("""
        INSERT INTO courses
        VALUES (?,?,?,?,?,?)""",(id,course_name,student,number_of_students,class_id,course_length))

    conn.commit()


def parseAndCompute(line, conn):
    line.replace(" ", "")
    line.replace("\n", "")
    lineInfo = line.split(",")
    table_name = lineInfo[0]

    if table_name == "S":
        insertStudent(lineInfo[1].strip(),lineInfo[2].strip(),conn)
    elif table_name == "R":
        insertClassroom(lineInfo[1].strip(),lineInfo[2].strip(),conn)
    elif table_name == "C":
        insertCourse(lineInfo[1].strip(),lineInfo[2].strip(),lineInfo[3].strip(),lineInfo[4].strip(),lineInfo[5].strip(),lineInfo[6].strip(),conn)

def print_tables(cursor):
    cursor.execute("SELECT * FROM courses")
    courses = cursor.fetchall()
    print("courses")
    for course in courses:
        print(course)

    cursor.execute("SELECT * FROM classrooms")
    classrooms = cursor.fetchall()
    print("classrooms")
    for classroom in classrooms:
        print (classroom)

    cursor.execute("SELECT * FROM students")
    students = cursor.fetchall()
    print("students")
    for student in students:
        print(student)






if not os.path.isfile('schedule.db'):  # the db does not exists, parse the config file and create the database
    conn = sqlite3.connect('schedule.db')
    cursor = conn.cursor()
    createTables(conn)
    textFile = sys.argv[1]+".txt"
    for line in open(textFile):
        parseAndCompute(line,conn)

    print_tables(cursor)
    cursor.close()
    conn.close()




